require('rootpath')()

const wso2Handler = require('lib/wso2/publisher/handler')
const publisherConfig = require('lib/wso2/publisher/config').data

let apiURI = publisherConfig.host + ':'
    + publisherConfig.port 
    + publisherConfig.endpoint
    + publisherConfig.version;

/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    /*************/
    /* List APIS */
    /*************/
    var list = function (request, response, next){
        
        let url = apiURI + publisherConfig.operation_endpoint.apis
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /***********/
    /* GET API */
    /***********/
    var get = function (request, response, next){
        
        let url = apiURI + publisherConfig.operation_endpoint.apis + '/' + request.params.id
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        get : get,
        list : list
    }
    
})();