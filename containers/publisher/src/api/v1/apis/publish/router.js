'use strict'

const router = require('express').Router({mergeParams:true})

const controller = require('./controller')

router.route('/')
    .post(controller.post)


module.exports = router