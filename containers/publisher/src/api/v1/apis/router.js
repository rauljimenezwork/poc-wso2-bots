'use strict'

const router = require('express').Router()
const publish = require('./publish/router')
const upgrade = require('./upgrade/router')
const subscription = require('./subscription/router')


const controller = require('./controller')


router.route('/')
    .get(controller.list)

router.route('/:id')
    .get(controller.get)

router.use('/:id/publish',publish)
router.use('/:id/upgrade',upgrade)
router.use('/:id/subscription',subscription)


module.exports = router