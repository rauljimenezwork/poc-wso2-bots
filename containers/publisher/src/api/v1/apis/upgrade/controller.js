require('rootpath')()

const wso2Handler = require('lib/wso2/publisher/handler')
const publisherConfig = require('lib/wso2/publisher/config')

let apiURI = publisherConfig.host + ':'
    + publisherConfig.port 
    + publisherConfig.endpoint
    + publisherConfig.version;

/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    /******************/
    /*  */
    /******************/
    var post = function (request, response, next){
        
        let uri = apiURI 
            + publisherConfig.operation_endpoint.version
            + '?apiId=' + request.params.id
            + '&newVersion=' + request.query.version
        
        wso2Handler.postToAPI(uri)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
    
    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        post : post
    }
    
})();