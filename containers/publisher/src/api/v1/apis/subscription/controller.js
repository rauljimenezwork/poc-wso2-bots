require('rootpath')()

const wso2Handler = require('lib/wso2/publisher/handler')
const publisherConfig = require('lib/wso2/publisher/config').data

let apiURI = publisherConfig.host + ':'
    + publisherConfig.port 
    + publisherConfig.endpoint
    + publisherConfig.version;

/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    
    /******************/
    /*  */
    /******************/
    var get = function (request, response, next){
        
        let uri = apiURI 
            + publisherConfig.operation_endpoint.subscription
            + '?apiId=' + request.params.id
            

        wso2Handler.getFromAPI(uri)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })

    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        get : get
    }
    
})();