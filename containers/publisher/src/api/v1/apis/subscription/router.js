'use strict'

const router = require('express').Router({mergeParams:true})

const controller = require('./controller')

router.route('/')
    .get(controller.get)


module.exports = router