'use strict'

const router = require('express').Router()
const unblock = require('./unblock/router')
const block = require('./block/router')


const controller = require('./controller')


router.route('/')
    .get(controller.list)

router.route('/:id')
    .get(controller.get)

router.use('/:id/unblock',unblock)
router.use('/:id/block',block)


module.exports = router