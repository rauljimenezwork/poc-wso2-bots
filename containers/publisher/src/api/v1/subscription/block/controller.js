require('rootpath')()

const wso2Handler = require('lib/wso2/publisher/handler')
const publisherConfig = require('lib/wso2/publisher/config').data
let apiURI = publisherConfig.host + ':'
    + publisherConfig.port 
    + publisherConfig.endpoint
    + publisherConfig.version;

/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    
    /******************/
    /*  */
    /******************/
    var post = function (request, response, next){
        
        let uri = apiURI 
            + publisherConfig.operation_endpoint.block_subscription
            + '?apiId=' + request.params.id
            + '&blockState=' + request.query.blockState
            

        wso2Handler.postToAPI(uri)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        post : post
    }
    
})();