require('rootpath')()

const wso2Handler = require('lib/wso2/publisher/handler')
const publisherConfig = require('lib/wso2/publisher/config').data


let apiURI = publisherConfig.host + ':'
    + publisherConfig.port 
    + publisherConfig.endpoint
    + publisherConfig.version;


/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    
    
    /**********************/
    /* List Subscriptions */
    /**********************/
    var list = function (request, response, next){
        
        let url = apiURI + publisherConfig.operation_endpoint.subscription
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /**********************/
    /* List Subscriptions */
    /**********************/
    var get = function (request, response, next){
        
        let url = apiURI + publisherConfig.operation_endpoint.subscription + '/' + request.params.id
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        get : get,
        list : list
    }
    
})();