'use strict'

const router = require('express').Router()


router.use('/apis', require('./apis/router'))
router.use('/subscription', require('./subscription/router'))



module.exports = router