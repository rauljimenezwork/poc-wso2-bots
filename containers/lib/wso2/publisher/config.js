const config = {
    
    host : 'https://10.208.74.38', // TODO MOVE TO ENV
    port : 9443,
    endpoint : '/api/am/publisher/',
    version : 'v0.13/',
    operation_endpoint : {
        apis : 'apis',
        publish : 'apis/change-lifecycle',
        version : 'apis/copy-api',
        subscription : 'subscriptions',
        block_subscription : 'block-subscription', 
        unblock_subscription : 'unblock-subscription'
    }
    
    
}

module.exports = (function (){
    
    
    var getURIFromEndPoint = function (operation) {
        
        
        let apiURI = config.host + ':'
            + config.port 
            + config.endpoint
            + config.version;

        
        switch (operation){
                
            case 'apis':
                return apiURI + config.operation_endpoint.apis;
                break;
                
            case 'publish':
                return apiURI + config.operation_endpoint.publish;
                break;
                
            case 'version':
                return apiURI + config.operation_endpoint.version;
                break;
                
            case 'subscription':
                return apiURI + config.operation_endpoint.subscription;
                break; 
            
            default :
                return apiURI
                break;
                
                
        }
        
        
    }
    
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        getURIFromEndPoint : getURIFromEndPoint,
        data : config
    }
    
})();
