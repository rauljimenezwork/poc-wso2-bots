var rp = require('request-promise')

let storeUser = 'rtjimenez';
let storePassword = 'pwd12345'

var consumeAPI = function (uri, method, data) {
    
    var options = {
        url: uri,
        method: method,
        headers: {
            Authorization: ' Bearer ' + data.access_token
        }
    }
    
    
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    return rp(options)
    .then ( (data) => {
        return JSON.parse(data)
    })
    .catch ((error) => {
        console.log(error)
        throw new Error ({'error' :error})
    })
    
}


var getToken = function (data) {
    
    var username = data.clientId
    var password = data.clientSecret
    
    var auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');
    
    let options = {
        url: 'https://10.208.74.38:8243/token',
        method: 'POST',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization" : auth,
        },
        form : {
            grant_type : 'password',
            username : storeUser,
            password : storePassword,
            scope : 'apim:api_view'
        }
        
    }
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    
    return rp(options)
    .then ( (data) => {
        return data
    })
    .catch ((error) => {
        console.log(error)
        throw new Error ({'error' :error})
    })
    
    
}




var getClientIDSecret = function ()  {
    
    let data = {
        "clientName": "rest_api_publisher",
        "owner": storeUser,
        "grantType": "password refresh_token",
        "saasApp": true
    }
    
    var username = storeUser
    var password = storePassword
    var auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');
    
    let options = {
        url: 'https://10.208.74.38:9443/client-registration/v0.13/register',
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization" : auth,
        },
        json  : data
    }
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    
    return rp(options)
    .then ( (data) => {
        return data
    })
    .catch ((error) => {
        console.log(error)
        throw new Error ({'error' :error})
    })
}




module.exports = (function (){
     
    var getFromAPI = function(uri) {
        
        return getClientIDSecret()
        .then (  ( data ) => {
            return getToken (data)
        })
        .then ( (token) => {
            return consumeAPI(uri, 'GET', JSON.parse(token))
        })  
        .then ((data) => {
            return data
        })
        .catch ( (error) => {
            console.log(error)
        })
    }
        
        
    var postToAPI = function(uri) {
        
        return getClientIDSecret()
        .then (  ( data ) => {
            return getToken (data)
        })
        .then ( (token) => {
            return consumeAPI(uri, 'POST', JSON.parse(token), )
        })  
        .then ((data) => {
            return data
        })
        .catch ( (error) => {
            console.log(error)
        })
    }
    

    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        getFromAPI : getFromAPI,
        postToAPI : postToAPI
    }
    
})();


