'use strict'

const express = require('express');
const compression = require('compression')
const path = require('path')
const app = express();
const api = require('./api'); // not necessary to  include index.js
const bodyParser = require('body-parser');


app.use(compression())
app.use(bodyParser.json());

app.get('/', (request, response, next) =>{
    response.send('working')
})

app.use('/api', api)

module.exports = app