'use strict'

const router = require('express').Router()

router.use('/apis', require('./apis/router'))


module.exports = router