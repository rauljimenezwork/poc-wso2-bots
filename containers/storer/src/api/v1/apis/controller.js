require('rootpath')();

const wso2Handler = require('lib/wso2/store/handler')
const storeConfig = require('lib/wso2/store/config').data


let apiURI = storeConfig.host + ':'
    + storeConfig.port 
    + storeConfig.endpoint
    + storeConfig.version;

/*********************/
/* exports functions */
/*********************/
module.exports = (function (){
     
    
    
    
    /******************/
    /* List Customers */
    /******************/
    var get = function (request, response, next){
        
        let url = apiURI + storeConfig.operation_endpoint.apis + '/' + request.params.id
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
    }
    
    
    /******************/
    /* List APIS */
    /******************/
    var list = function (request, response, next){
       
        let url = apiURI + storeConfig.operation_endpoint.apis
        
        wso2Handler.getFromAPI(url)
        .then ( (data) => {
            response.json(data)
        })
        .catch ( (error) => {
            response.send(error).status(500)
        })
        
        
    }
    
    
    /*****************/
    /*    Return     */
    /*****************/
    return {
        get : get,
        list : list
    }
    
})();