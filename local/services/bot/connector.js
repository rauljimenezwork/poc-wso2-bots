require ('rootpath')()

const appId = '58665151-2d62-4b93-80e2-d66f42bc6e12'
const appPassword = 'ulyGCS57800}@$brnuDSQC^'

const rp = require ('request-promise')

var siteUrl = require('./site-url');
var builder = require('botbuilder');

var inMemoryStorage = new builder.MemoryBotStorage();

var connector = new builder.ChatConnector({
    appId: appId,
    appPassword: appPassword
});


var connectorListener = connector.listen();

function listen() {
    return function (req, res) {
        // Capture the url for the hosted application
        // We'll later need this url to create the checkout link
        var url = req.protocol + '://' + req.get('host');
        siteUrl.save(url);
        connectorListener(req, res);
    };
}


var bot = new builder.UniversalBot(connector,
                                  (session) =>{
    
    //session.beginDialog('welcome:/')
    
    session.send('Hola, ¿en qué puedo ayudarte?')

})
.set('storage', inMemoryStorage); // Register in-memory storage


const luisApp = 'f215f194-43e5-461b-93f3-af65e319bf09'
const luisKey = '31de3cce90564425b24ccebbe80bb4fb'

const LuisModelUrl  = `https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/f215f194-43e5-461b-93f3-af65e319bf09?subscription-key=7abfdbbaf46c49379f88efad083e9f40&timezoneOffset=-360&q=`

// Creamos el recognizer que coge las intents de LUIS y se añaden al bot
var recognizer = new builder.LuisRecognizer(LuisModelUrl);
var dialog = new builder.IntentDialog({recognizers: [recognizer]})


var apiDialog = require('./dialogs/apis')

bot.library(apiDialog.clone())

/*
bot.library(require('./dialogs/apis').createLibrary());

bot.dialog('ApiDialog',
    (session) => {
        session.send('You reached the APIS intent. You said \'%s\'.', session.message.text);
        session.beginDialog('apis:/')
    }
).triggerAction({
    matches: 'ApiDialog'
})


bot.on('conversationUpdate', function (message) {
    if (message.membersAdded) {
        message.membersAdded.forEach(function (identity) {
            if (identity.id === message.address.bot.id) {
                bot.beginDialog(message.address, '/');
            }
        });
    }
});
*/



module.exports.listen = listen;