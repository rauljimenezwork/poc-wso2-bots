var builder = require('botbuilder');
const rp = require('request-promise')

const libName = 'apis'

function checkText (text){
   
    var options = {
        uri: "http://localhost:20002/api/v1/apis",
        method: "GET",
        json: true
    }
    
    return rp(options)
    
}

var lib = new builder.Library(libName);

lib.dialog('/', [
    (session) =>{
        session.send("Hola!!");
        builder.Prompts.text(session,'Dime algo')
    },
    async function (session, results) {
        session.send(`Oido cocina: ${results.response}`);
        
        var text = await checkText(results.response)
        session.send(`${JSON.stringify(text)}`);
        
        session.endDialog();
    }

]);

// Export createLibrary() function
module.exports.createLibrary = function () {
    return lib.clone();
};