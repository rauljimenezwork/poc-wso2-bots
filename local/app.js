'use strict'

const express = require('express');
const compression = require('compression')

const app = express();
const api = require('./api'); // not necessary to  include index.js
const bodyParser = require('body-parser');


app.use(compression())
app.use(bodyParser.json());

// Have to respond swagger with api information
app.get('/', (req, res) => {
  res.send('API is working great!!')
})

app.use('/api', api)

module.exports = app