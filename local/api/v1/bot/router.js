'use strict'

const router = require('express').Router()


let connector = require('services/bot/connector');

router.route('/')
    .post(connector.listen());



module.exports = router