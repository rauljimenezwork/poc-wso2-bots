'use strict'

const router = require('express').Router()

router.use('/bot', require('./bot/router'))

module.exports = router

